EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4300 3350 1050 1050
U 5EB2CB39
F0 "controller" 50
F1 "controller.sch" 50
F2 "pwm_out" I R 5350 3450 50 
$EndSheet
$Sheet
S 6050 3350 1050 1050
U 5EB349B8
F0 "motor-driver" 50
F1 "motor-driver.sch" 50
F2 "pwm_in" I L 6050 3450 50 
$EndSheet
$Comp
L power:VCC #PWR04
U 1 1 5EB59BCA
P 6650 2650
F 0 "#PWR04" H 6650 2500 50  0001 C CNN
F 1 "VCC" H 6667 2823 50  0000 C CNN
F 2 "" H 6650 2650 50  0001 C CNN
F 3 "" H 6650 2650 50  0001 C CNN
	1    6650 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5EB59CE4
P 4800 2650
F 0 "#PWR01" H 4800 2400 50  0001 C CNN
F 1 "GND" H 4805 2477 50  0000 C CNN
F 2 "" H 4800 2650 50  0001 C CNN
F 3 "" H 4800 2650 50  0001 C CNN
	1    4800 2650
	1    0    0    -1  
$EndComp
$Comp
L power:GNDPWR #PWR02
U 1 1 5EB59F4A
P 5500 2650
F 0 "#PWR02" H 5500 2450 50  0001 C CNN
F 1 "GNDPWR" H 5504 2496 50  0000 C CNN
F 2 "" H 5500 2600 50  0001 C CNN
F 3 "" H 5500 2600 50  0001 C CNN
	1    5500 2650
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR03
U 1 1 5EB5A2D3
P 5850 2650
F 0 "#PWR03" H 5850 2500 50  0001 C CNN
F 1 "+12V" H 5865 2823 50  0000 C CNN
F 2 "" H 5850 2650 50  0001 C CNN
F 3 "" H 5850 2650 50  0001 C CNN
	1    5850 2650
	1    0    0    -1  
$EndComp
$Comp
L Device:Net-Tie_2 NT1
U 1 1 5EB5A9CA
P 5150 2600
F 0 "NT1" H 5150 2781 50  0000 C CNN
F 1 "Net-Tie_2" H 5150 2700 50  0000 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 5150 2600 50  0001 C CNN
F 3 "~" H 5150 2600 50  0001 C CNN
	1    5150 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2600 4800 2650
Wire Wire Line
	4800 2600 5050 2600
Wire Wire Line
	5500 2600 5500 2650
Wire Wire Line
	5250 2600 5500 2600
$Comp
L Device:Net-Tie_2 NT2
U 1 1 5EB5CB4D
P 6250 2700
F 0 "NT2" H 6250 2881 50  0000 C CNN
F 1 "Net-Tie_2" H 6250 2800 50  0000 C CNN
F 2 "NetTie:NetTie-2_SMD_Pad0.5mm" H 6250 2700 50  0001 C CNN
F 3 "~" H 6250 2700 50  0001 C CNN
	1    6250 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 2650 5850 2700
Wire Wire Line
	5850 2700 6150 2700
Wire Wire Line
	6350 2700 6650 2700
Wire Wire Line
	6650 2700 6650 2650
Wire Wire Line
	5350 3450 6050 3450
$Comp
L Mechanical:MountingHole H1
U 1 1 5EC24ABA
P 3300 3550
F 0 "H1" H 3400 3596 50  0000 L CNN
F 1 "MountingHole" H 3400 3505 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3300 3550 50  0001 C CNN
F 3 "~" H 3300 3550 50  0001 C CNN
	1    3300 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5EC257CA
P 7600 3550
F 0 "H2" H 7700 3596 50  0000 L CNN
F 1 "MountingHole" H 7700 3505 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 7600 3550 50  0001 C CNN
F 3 "~" H 7600 3550 50  0001 C CNN
	1    7600 3550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5EB7D04E
P 3300 3850
F 0 "H3" H 3400 3896 50  0000 L CNN
F 1 "MountingHole" H 3400 3805 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 3300 3850 50  0001 C CNN
F 3 "~" H 3300 3850 50  0001 C CNN
	1    3300 3850
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5EB7D79D
P 7600 3850
F 0 "H4" H 7700 3896 50  0000 L CNN
F 1 "MountingHole" H 7700 3805 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.5mm_Pad" H 7600 3850 50  0001 C CNN
F 3 "~" H 7600 3850 50  0001 C CNN
	1    7600 3850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
